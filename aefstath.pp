class hg_playground::aefstath($myenv_variable = "default") {
  notify{"collectd plugin is in da house":}

  include afs
  include sssd
  include collectd::plugin::csv
  include ::cerncollectd::plugin::session

  package{'training-sensor':
    ensure => present
  }

  file{'/etc/profile.d/mystuff_aefstath.sh':
    owner   => root,
    group   => root,
    mode    => '0644',
    content => template('hg_playground/mystuff_aefstath.erb')

 }
}

