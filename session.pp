class cerncollectd::plugin::session(

  Integer $interval = $cerncollectd::default_interval,
) inherits cerncollectd {

  include ::collectd

  package { 'collectd-session':
    ensure => present,
  }

  collectd::plugin::python::module { 'session':
    ensure  => present,
    config  => [{
      'Interval' =>  "${interval} "
    }],
  }
}
